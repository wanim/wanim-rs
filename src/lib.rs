use std::f64::consts::PI;

use wasm_bindgen::prelude::*;
use web_sys::CanvasRenderingContext2d;

type JsResult<T> = Result<T, JsValue>;

fn dyn_sample(f: &impl Fn(f64) -> f64, start: u32, end: u32, res: f64) -> (Vec<f64>, Vec<f64>) {
    fn sample_segment(
        f: &impl Fn(f64) -> f64,
        seg: [(f64, f64); 3],
        res: f64,
        depth: usize,
    ) -> Vec<(f64, f64)> {
        if depth == 0 {
            return seg.into_iter().collect();
        }

        let v1 = (seg[1].0 - seg[0].0, seg[1].1 - seg[0].1);
        let v2 = (seg[2].0 - seg[1].0, seg[2].1 - seg[1].1);

        let diff = (f64::atan2(v1.1, v1.0) - f64::atan2(v2.1, v2.0)).abs();

        if diff > res {
            let new_x_l = (seg[0].0 + seg[1].0) / 2.0;
            let new_x_r = (seg[1].0 + seg[2].0) / 2.0;

            let new_p_l = (new_x_l, f(new_x_l));
            let new_p_r = (new_x_r, f(new_x_r));

            let sampled_left = sample_segment(f, [seg[0], new_p_l, seg[1]], res, depth - 1);
            let mut sampled_right = sample_segment(f, [seg[1], new_p_r, seg[2]], res, depth - 1);
            sampled_right.remove(0);
            return [sampled_left, sampled_right].concat();
        }

        seg.into_iter().collect()
    }

    const INITIAL_POINTS: usize = 16;

    let step: usize = ((end - start) as usize) / INITIAL_POINTS;

    let mut ps: Vec<(f64, f64)> = (start..(end + step as u32))
        .step_by(step)
        .map(|x| {
            let x = x as f64;
            (x, f(x))
        })
        .collect();

    const DEPTH: usize = 4;

    let mut i = 0;
    loop {
        if i >= ps.len() - 2 {
            break;
        }
        let p1 = ps[i];
        let p2 = ps[i + 1];
        let p3 = ps[i + 2];

        let new_vec = sample_segment(f, [p1, p2, p3], res, DEPTH);

        ps.remove(i);
        ps.remove(i);
        ps.remove(i);

        for (k, v) in new_vec.iter().enumerate() {
            ps.insert(i + k, *v)
        }

        i += new_vec.len() - 1;
    }

    ps.into_iter().unzip()
}

#[wasm_bindgen]
pub fn plot_function(f: &str, context: CanvasRenderingContext2d) -> JsResult<()> {
    const START: u32 = 0;
    const END: u32 = 800;

    let f: meval::Expr = f
        .parse()
        .map_err(|e| format!("Unable to parse expression: {:?}", e))?;
    let f = f
        .bind("x")
        .map_err(|e| format!("Unable to bind x to expression: {:?}", e))?;

    context.set_fill_style(&"#FFFFFF".into());

    let canvas = context
        .canvas()
        .ok_or("Unable to get canvas from context")?;
    context.fill_rect(0.0, 0.0, canvas.width() as f64, canvas.height() as f64);

    let (xs, ys) = dyn_sample(&f, START, END, 0.1 / 180.0 * PI);

    context.begin_path();
    context.move_to(xs[0], 600.0 - ys[0]);
    context.fill_rect(xs[0], 600.0 - ys[0], 4.0, 4.0);
    for i in 1..xs.len() {
        context.line_to(xs[i], 600.0 - ys[i]);
    }
    context.stroke();

    Ok(())
}
